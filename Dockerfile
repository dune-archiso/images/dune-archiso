# Copyleft (c) November, 2022, Oromion.
# Usage:
# $ git clone git@gitlab.com:dune-archiso/images/dune-archiso.git
# $ cd dune-archiso
# $ docker build -t dune-archiso/dune-archiso:latest .

FROM archlinux:base-devel

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune Arch" \
  description="Dune in Arch" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso" \
  vendor="Oromion Aznarán" \
  version="1.0"

ARG GPG_KEY="2403871B121BD8BB"

# sed -i 's/ usr\/share\/doc\/\*//g' /etc/makepkg.conf && \
# sed -i 's/usr\/share\/man\/\* //g' /etc/makepkg.conf && \

RUN ln -s /usr/share/zoneinfo/America/Lima /etc/localtime && \
  sed -i 's/^#Color/Color/' /etc/pacman.conf && \
  sed -i '/#CheckSpace/a ILoveCandy' /etc/pacman.conf && \
  sed -i 's/^ParallelDownloads = 5/ParallelDownloads = 30/' /etc/pacman.conf && \
  sed -i 's/^#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/' /etc/makepkg.conf && \
  sed -i 's/^#BUILDDIR/BUILDDIR/' /etc/makepkg.conf && \
  sed -i 's/purge debug lto/purge !debug !lto/' /etc/makepkg.conf && \
  sed -i 's/^#PACKAGER=\"John Doe <john@doe.com/PACKAGER=\"Carlos Aznarán <caznaranl@uni.pe/' /etc/makepkg.conf && \
  sed -i 's/^#GPGKEY=\"/GPGKEY=\"3880BDA725DE92E7BE7C1A2F6225FD2615EB3DEE/' /etc/makepkg.conf && \
  echo -e '\n[multilib]\nInclude = /etc/pacman.d/mirrorlist' | tee -a /etc/pacman.conf && \
  pacman-key --init && \
  pacman-key --populate archlinux && \
  pacman-key --recv-keys ${GPG_KEY} && \
  pacman-key --finger ${GPG_KEY} && \
  pacman-key --lsign-key ${GPG_KEY} && \
  pacman --needed --noconfirm --noprogressbar -Sy archlinux-keyring && \
  pacman --needed --noconfirm --noprogressbar -Syuq && \
  pacman -Qtdq | xargs -r pacman --noconfirm -Rscn && \
  pacman -Scc <<< Y <<< Y && \
  rm -r /var/lib/pacman/sync/* && \
  useradd -l -md /home/aur -s /bin/bash aur && \
  passwd -d aur && \
  echo 'aur ALL=(ALL) ALL' > /etc/sudoers.d/aur

USER aur

RUN mkdir -p ~/.gnupg && \
  echo 'standard-resolver' > ~/.gnupg/dirmngr.conf && \
  chmod 600 ~/.gnupg/* && \
  chmod 700 ~/.gnupg && \
  gpg --keyserver hkp://keys.openpgp.org:80 --recv-keys E5B47782DE09813BCC3518E159DA94F1FC8FD313 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys ABE52C516431013C5874107C3F71FE0770D47FFB && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 2AA99AA4E2D6214E6EA01C9A4AF42916F6E5B1CF && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 703607A1FD9AF4205E735522B95BE0EFB19724A1 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 900A958D9A0ACA58B1468F2471AA298BCA171145 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D3A93CAD751C2AF4F8C7AD516C35B99309B5FA62 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 13975A70E63C361C73AE69EF6EEB81F8981C74C7 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 6FB8B07A620CC7A7FB5B2AB4110D6C98E760BEF2 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CF8A16EF6290224A42B850BD69F3C4912FEA5FDE && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4E6E6521EF300E30457D7AFB8AAAC30E7088EFCB && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E9762343DA3C836A78CD254A6A63028F5A74E893 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 53F047CE66B91B0F724C545D5C86C0E4211D5B8E && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys FE66471B43559707AFDAD955DE7A44FAC7FB382D && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 1F521FF0F87E9E1CDE46B8A9F4928C4DD24D4DF8 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 51A0F4A0C8CFC98F842EA9A8B94556F81C85D0D5 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 95D2E9AB8740D8046387FD151A09227B1F435A33 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 79151C2E6351E7278DA1A730BF38D4D02A328DFF && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 33EE4AA14D584BFA72AFD02D87682820AADECB71 && \
  gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys DBDC63E97C526204335805941FA7A782EC90634E && \
  curl -s https://github.com/thombashi.gpg | gpg --import && \
  curl -s https://github.com/lukeolson.gpg | gpg --import && \
  curl -s https://github.com/MakisH.gpg | gpg --import && \
  curl -s https://github.com/web-flow.gpg | gpg --import

CMD ["/bin/bash"]
